# Programavimo kalbų teorijos lab. darbai

### Apie repozitoriją

Čia kaupiami 2016 PKT modulio studentų laboratoriniai darbai.

### Laboratorinių darbų pateikimas

Kiekvienas studentas sukuria katalogą, kuriame yra 4 katalogai su pavadinimais: 1,2,3,4. Kiekviename iš jų reikės įkelti po atliktą užduotį kiekvienam laboratoriam darbui.

NEKELKITE SAVO PROJEKTŲ FAILŲ! Užtenka vieno kodo failo su sprendimu.